# iDBTrans

iDBTrans is a software system with the  purpose of assisting database migration processes from Oracle to PostgreSQL. Among the elements that can be migrated are tables, primary and foreing keys, indexes, functions, procedures, etc.

iDBTrans ameliorates the performance of similiar tools in the market and as these cannot be guaranteed a full automatic migration. This software was developed at [Infotec](https://www.infotec.mx/), a Public Research Center of Conacyt in México, using Open JDK 8 and an extension of the [JSQLParser](https://github.com/JSQLParser/JSqlParser) library.


## Getting Started

To execute iDBTrans, two configuration files located in the **config** directory have to be filled, namely **config_source.json** and **config_target.json**. These files set the data for the source database and the corresponding output database. This is a rather simple method which provides agility in the learning curve for the usage of iDBTrans. Examples of both files are listed below (see [Samples](sample-a.md) section):


General config_source.json:
```
 {
    "url":"jdbc:oracle:thin:@HOST_NAME:PORT:SDI",
    "user":"YOUR_USERNAME",
    "password":"YOUR_PASSWORD",		
    "driver":"oracle.jdbc.driver.OracleDriver",
    "schema":"YOUR_DATABASE_SCHEMA_NAME_TO_MIGRATE"
 }

```


General config_tarjet.son:
```
 {
    "url":"jdbc:postgresql://HOST_NAME:PORT/DATABASENAME",
    "user":"YOUR_USERNAME",
    "password":"YOUR_PASSWORD",
    "driver":"org.postgresql.Driver",
    "schema":"YOUR_DATABASE_SCHEMA_NAME_MIGRATED"
 }

```
Once the input and output database files have been configured, the following commands must be executed to perform the migration.

```
cd bin
java -jar idbtrans.jar 
```
A description of the performed operations is displayed in the shell along with some relevant statistics.

```
--------------------------------------------------------------------------------------
--   ______   ____    ____    ______
--  /\__  _\ /\  _`\ /\  _`\ /\__  _\
--  \/_/\ \/ \ \ \/\ \ \ \L\ \/_/\ \/ _ __    __      ___     ____
--     \ \ \  \ \ \ \ \ \  _ <' \ \ \/\`'__\/'__`\  /' _ `\  /',__\
--      \_\ \__\ \ \_\ \ \ \L\ \ \ \ \ \ \//\ \L\.\_/\ \/\ \/\__, `\
--      /\_____\\ \____/\ \____/  \ \_\ \_\\ \__/.\_\ \_\ \_\/\____/
--      \/_____/ \/___/  \/___/    \/_/\/_/ \/__/\/_/\/_/\/_/\/___/
--
--                            Version  0.0.0
--                         DAIC -  INFOTEC CDMX

--------------------------------------------------------------------------------------

Loading dictionary...
Loading source data base file configuration...
Loading tarjet data base file configuration...
Connecting to source data base...
Connecting to tarjet data base...
Started at:Fri Mar 27 03:36:43 CST 2020 getting sequences...
Started at:Fri Mar 27 03:36:43 CST 2020 getting default values column...
Started at:Fri Mar 27 03:36:44 CST 2020 getting partitions...
Started at:Fri Mar 27 03:36:45 CST 2020 getting tables...
Started at:Fri Mar 27 03:36:45 CST 2020 Getting columns...
Started at:Fri Mar 27 03:37:55 CST 2020 getting Views...
Started at:Fri Mar 27 03:37:55 CST 2020 getting Materialized Views...
Started at:Fri Mar 27 03:37:55 CST 2020 getting Primary Keys...
Started at:Fri Mar 27 03:37:57 CST 2020 getting Foreign keys...
Started at:Fri Mar 27 03:39:32 CST 2020 getting constraints type check...
Started at:Fri Mar 27 03:39:33 CST 2020 getting indexes...
Started at:Fri Mar 27 03:39:33 CST 2020 getting objects code, procedures,packages,funtions...
Started at:Fri Mar 27 03:39:36 CST 2020 getting dbaObjects...
Started at:Fri Mar 27 03:39:36 CST 2020 getting pck procedures....
Started at:Fri Mar 27 03:39:36 CST 2020 getting code procedures package...
Started at:Fri Mar 27 03:39:38 CST 2020 Set code to procedures and functions ..
Started at:Fri Mar 27 03:39:52 CST 2020 Set code to triggers..
Started at:Fri Mar 27 03:39:53 CST 2020 Set objects type user defined for every schema..
Started at:Fri Mar 27 03:39:53 CST 2020 Setting status VALID or INVALID for objects of code
Start of migration...
Creating schemas and script for PostgreSQL..
Creating Sequences and script for PostgreSQL..
Creating tables and script for PostgreSQL..
Creating default values for tables and script for PostgreSQL..
Creating partitions and script for PostgreSQL..
Creating Indexes and script for PostgreSQL..
Creatting Functions and script for PostgreSQL..
Creatting Procedures and script for PostgreSQL..
Creatting packages and script for PostgreSQL..
Creatting triggers and script for PostgreSQL..
Creatting views and script for PostgreSQL..
Creatting materialized views and script for PostgreSQL..
Inserting data on tables for PostgreSQL..
Started at:Fri Mar 27 03:40:14 CST 2020
SCHEMA: SIO inserted rows:4
No. of Schemas: 1
-----------------------------------------
Infotec_Ora2Post: Migration Report
-----------------------------------------
        Input Data Base
-----------------------------------------
*********** BEGIN SCHEMA: SIO [1] de [1] ***********
No. of Tables to migrate : 807
No. of Indexes to migrate : 354
No. of Column Default: 603
No. of Views: 141
Status: VALIDS 122, INVALIDS 19
No. of Materilized Views: 39
Status: VALIDS 0, INVALIDS 39
No. of Functions: 996
Status: VALIDS 900, INVALIDS 96
No. of Procedures: 509
Status: VALIDS 374, INVALIDS 135
Packages :35 VALIDS : 24 INVALIDS:11 No. of Function and Procedures VALIDS: 366
No. of Triggers: 122 VALIDS: 80 INVALIDS:42
*********** END SCHEMA SIO ***********
-----------------------------------------
        Output Data Base
-----------------------------------------
No. of Schemas: 1
*********** BEGIN SCHEMA: SIO [1] de [1] ***********
No. of Tables: 824
        Status: Migrated 806 No migrated 18
No. of Index: 354
        Status: Migrated 311, No migrated 43
No. of Column Default: 603
        Status: Migrated 595, No migrated 8
No. of Views: 141
        Status: Migrated 89, No migrated 52 Converted:122 Not-converted:19
No. of Materialized Views: 39
        Status: Migrated 19, No migrated 20 Converted:30 Not-converted:9
No. of Functions: 996
        Status: Migrated 834, No migrated 162 Converted:0 Not-converted:996
No. of Procedures: 509
        Status: Migrated 298, No migrated 211 Converted:368 Not-converted:141
No. of Functions and Procedures Packages: 35
ALL PROCEDURES MIGRATED :0 NO MIGRATED:0
No. of Triggers: 122
        Status: Migrated 70, No migrated 52 Converted:80 Not-converted:42
*********** END SCHEMA SIO ***********
-----------------------------------------
Ended process...
Thanks...
```


For more run options write (see [Customization](customization.md) section):

```
java -jar idbtrans.jar -h
```
yields some useful help information about command line parameters:

```
--------------------------------------------------------------------------------------
--   ______   ____    ____    ______
--  /\__  _\ /\  _`\ /\  _`\ /\__  _\
--  \/_/\ \/ \ \ \/\ \ \ \L\ \/_/\ \/ _ __    __      ___     ____
--     \ \ \  \ \ \ \ \ \  _ <' \ \ \/\`'__\/'__`\  /' _ `\  /',__\
--      \_\ \__\ \ \_\ \ \ \L\ \ \ \ \ \ \//\ \L\.\_/\ \/\ \/\__, `\
--      /\_____\\ \____/\ \____/  \ \_\ \_\\ \__/.\_\ \_\ \_\/\____/
--      \/_____/ \/___/  \/___/    \/_/\/_/ \/__/\/_/\/_/\/_/\/___/
--
--                            Version  0.0.0
--                         DAIC -  INFOTEC CDMX

--------------------------------------------------------------------------------------

Usage: <main class> [options]
  Options:
    -d, --debug
      Enable debug mode
      Default: false
    -h, --help
      Help/Usage
    -i, --input
      Source data base config file
      Default: ../config/config_source.json
    -o, --out
      Tarjet data base config file
      Default: ../config/config_tarjet.json
    -s, --statistics
      Statistics description
      Default: false
```


## Data Import
To import the data (registers) it is necessary to define a json file (in output_scripts folder) with the basic structure of the database, which includes the list of schemas, with tables and their columns with number of registers to import, an examples is shown below (it can be automatic generated by the interface):
```
{
  "lstSchemasTable" : [ {
    "schemaName" : "YOUR_SCHEMA_NAME",
    "tableList" : [ {
      "tableName" : "TABLE_NAME",
      "columnList" : [ {
        "schemaName" : "YOUR_SCHEMA_NAME",
        "tableName" : "TABLE_NAME",
        "columnName" : "COLUMN_NAME",
        "dataType" : "DATA_TYPE",
        ...
           }, {
        "schemaName" : "YOUR_SCHEMA_NAME",
        "tableName" : "TABLE_NAME",
        "columnName" : "COLUMN_NAME",
        "dataType" : "DATA_TYPE",
        ...
      }]
      "fkList" : [ ],
      "constraintList" :"" ,
      "comment" : "",
      "status" : "VALID",
      "statusText" : "",
      "hintStatus" : "",
      "sqlQuery" : "",
      "temporary" : false,
      "owner" : "YOUR_SCHEMA_NAME",
      "numRows" : "Number",
      "partitioned" : "NO",
      "rowsToExport" : "Number",
      "exportSelected" : true

      ...
    }]
    ...
  }]    
...
}
```

Once this file is defined the following commands must be executed to perform the regiters import.

```
cd bin
java -jar idbtrans-rwr.jar 
```
A description of the import process is displayed in the shell as follows:

```
 _ ____  ____ _____                          ______        ______  
 (_)  _ \| __ )_   _| __ __ _ _ __  ___      |  _ \ \      / /  _ \ 
 | | | | |  _ \ | || '__/ _` | '_ \/ __|_____| |_) \ \ /\ / /| |_) |
 | | |_| | |_) || || | | (_| | | | \__ \_____|  _ < \ V  V / |  _ < 
 |_|____/|____/ |_||_|  \__,_|_| |_|___/     |_| \_\ \_/\_/  |_| \_\
                                                                    

	 version 0.0.0
 	 DAIC-INFOTEC
Loading source data base file configuration...
Loading tarjet data base file configuration...
Connecting to source data base...
Connecting to tarjet data base...
Select and inserting data on tables from Oracle to PostgreSQL..
Started at:Thu Jul 16 23:24:17 CDT 2020
Inserting in Table CC_CLASE_AUXILIAR with 13 rows.
Inserting in Table ASIENTOS_DOCUMENTO with 256 rows.
Inserting in Table AD_USUARIO_ENTIDAD with 178 rows.
Inserting in Table CG_ARTICULO with 16 rows.
Inserting in Table CG_DEDUCCION_AUMENTO with 55 rows.
Inserting in Table CG_CONTRIBUYENTE with 1 rows.
Inserting in Table CG_CONCEPTO with 736 rows.
Inserting in Table CG_CATALOGO with 14 rows.
Inserting in Table CG_BENEFICIARIO with 11567 rows.
Inserting in Table CG_BANCOS with 27 rows.
Inserting in Table CC_PLAN_CUENTA with 695 rows.
Inserting in Table CC_NORMA_CONTABLE with 1 rows.
Inserting in Table CC_MATRIZ_RETENCIONES_CONTA with 46 rows.
Inserting in Table CC_MATRIZ_RELACION with 55 rows.
Inserting in Table CG_SUBCONCEPTO with 755 rows.
Inserting in Table CG_SERVICIO_ENTIDAD with 20 rows.
Inserting in Table CG_SERVICIO with 20 rows.
Inserting in Table CG_SECUENCIA_PROCESO with 7 rows.
Inserting in Table CG_EJERCICIO with 6 rows.
Inserting in Table CG_DOCUMENTO_RESPALDO with 215 rows.
Inserting in Table CG_DETALLE_SERVICIO_ENTIDAD with 143 rows.
Inserting in Table CG_DETALLE_SERVICIO with 151 rows.
Inserting in Table CG_DETALLE_CATALOGO with 1105 rows.
Inserting in Table CG_DEDUCCION_AUMENTO_ENTIDAD with 51 rows.
Inserting in Table TE_LBANCOS with 0 rows.
Inserting in Table TE_FORMATO_CHEQUE with 0 rows.
Inserting in Table TE_DETALLE_PAGO with 10 rows.
Inserting in Table TE_CUENTAS_TESORERIA with 6274 rows.
Inserting in Table DETALLE_DOCUMENTO with 3043 rows.
Inserting in Table DETALLE_CONCEPTO with 3 rows.
Inserting in Table DEDUCCIONES_DOCUMENTO with 4 rows.
Inserting in Table CT_OPERACIONES_BANCARIAS with 6 rows.
Inserting in Table CP_TIPO_GASTO with 3 rows.
Inserting in Table CP_RECURSO_AUXILIAR with 181 rows.
Inserting in Table CP_RECURSO with 61 rows.
Inserting in Table CP_PROYECTO with 1 rows.
Inserting in Table CP_OBJETO_GASTO with 613 rows.
Inserting in Table CP_ESTRUCTURA_CLAVE_EGRESOS with 36 rows.
Inserting in Table CG_TIPO_EXPEDIENTE with 10 rows.
Inserting in Table CG_TIPO_DOCUMENTO_PROCESO with 323 rows.
Inserting in Table CG_TIPO_DOCUMENTO_EVENTO with 70 rows.
Inserting in Table CG_TIPO_DOCUMENTO with 22 rows.
Inserting in Table CG_TIPO_DOC_IDENTIFICACION with 2 rows.
Inserting in Table CG_SUBFUNCION with 111 rows.
Inserting in Table CG_ESTRUCTURA_CUENTA with 6 rows.
Inserting in Table CG_ENTIDAD_FEDERATIVA with 1 rows.
Inserting in Table CG_EJERCICIO_ENTIDAD with 12 rows.
Inserting in Table INICIALIZACION_SALDOS_GASTO with 0 rows.
Inserting in Table MES with 12 rows.
Inserting in Table TRANSFERS with 0 rows.
Inserting in Table PRY_CT_ESPECIALIDAD with 4 rows.
Inserting in Table CG_URESPONSABLE with 42 rows.
Inserting in Table CG_PERSONA_COMPLEMENTO with 9707 rows.
Inserting in Table AUX_AMPLIACIONES with 0 rows.
Inserting in Table AUXILIAR_GENERICO with 6 rows.
Inserting in Table DOC_CONTABLE_REGISTRO with 3032 rows.
Inserting in Table CG_ESTATUS with 20 rows.
Inserting in Table DOC_METODO_PAGO with 35 rows.
Inserting in Table DOC_TIPODOC_CONTABLE with 12 rows.
Inserting in Table PRY_CT_COND_PAGO with 18 rows.
Inserting in Table CG_IMPUESTO_ENTIDAD with 8 rows.
Inserting in Table CG_IMPUESTO with 24 rows.
Inserting in Table CG_TIPO_ARTICULO with 109 rows.
Inserting in Table CG_PERSONA_PRINCIPAL with 11560 rows.
Inserting in Table CG_MUNICIPIO with 2 rows.
Inserting in Table CG_FUNCION with 32 rows.
Inserting in Table CG_FUENTE with 3 rows.
Inserting in Table CG_FINALIDAD with 4 rows.
Inserting in Table BASECAMBIOFECHAS with 0 rows.
Inserting in Table TMPMOVIMIENTOSPRESUPUESTO with 0 rows.
Inserting in Table CG_SECUENCIA with 17 rows.
Inserting in Table CG_RESPALDO_DOCUMENTO with 70 rows.
Inserting in Table RELACION_DOCUMENTO_PROCESO with 337 rows.
Inserting in Table TE_TALONARIO_CHEQUES with 0 rows.
Inserting in Table TE_SALDOS_CUENTA_EJERCICIO with 3620 rows.
Inserting in Table AUX with 0 rows.
Inserting in Table RPT_XML_CTAS_X_REPORTE with 0 rows.
Inserting in Table TMPINICIALES with 0 rows.
Inserting in Table CG_PROCESO with 115 rows.
Inserting in Table DOC_CONTABLE_CORREOS_CLIENTE with 752 rows.
Inserting in Table DOC_DETALLE_ANTERIORES with 12941 rows.
Inserting in Table DOC_CONTABLE_DETALLE with 7767 rows.
Inserting in Table DOC_CONTABLE_DET_CONSOLIDADO with 150 rows.
Inserting in Table CG_ENTIDAD with 2 rows.
Inserting in Table DOC_FACTURAS_ANTERIORES with 2508 rows.
Inserting in Table INI_PRESUPUESTO with 1265 rows.
Inserting in Table RH_DEVENGADO with 131 rows.
Inserting in Table PRY_PROYECTO_APROBACION with 425 rows.
Inserting in Table EJERCIDOS_PAGADOS_EJER_ANT with 228 rows.
Inserting in Table PRY_UR_FONDOSADMIN_VERSIONES with 524 rows.
Inserting in Table PRY_PROYECTO_UR_FONDOSADMIN with 84 rows.
Inserting in Table DOC_UNIDAD_MEDIDA_STD with 2 rows.
Inserting in Table DOC_USO_CFDI with 4 rows.
Inserting in Table DOC_TIPO_RELACION with 3 rows.
Inserting in Table DOC_TIPO_FACTOR with 3 rows.
Inserting in Table DOC_PRODUCTOS_SERVICIOS with 8 rows.
Inserting in Table DOC_FORMA_PAGO with 2 rows.
Inserting in Table DOC_IMPUESTO with 3 rows.
Inserting in Table DOC_MONEDA with 1 rows.
Inserting in Table PRY_APROBACION_PROYECTO with 514 rows.
Inserting in Table PRY_PROYECTO_OBSERV_RECHAZO with 203 rows.
Inserting in Table NUMERO_DOCUMENTO_RELACIONADO with 0 rows.
Inserting in Table DOC_CONTABLE_COMPROBANTES with 79 rows.
Inserting in Table DOC_COMP_PAGOS_IMPUESTOS with 0 rows.
Inserting in Table DOC_COMP_PAGOS_ENCABEZADO with 523 rows.
Inserting in Table DOC_COMP_PAGOS_DOCTOREL with 523 rows.
Inserting in Table DEVENGADOS_X_PAGAR_EA with 307 rows.
Inserting in Table DOC_TIPO_COMPROBANTE with 5 rows.
Inserting in Table DOC_TIPO_CADENA_PAGO with 0 rows.
Inserting in Table DOC_CONTABLE_CORREOS_AP with 1388 rows.
Inserting in Table PAIS with 1 rows.
Inserting in Table PRY_CT_FONDOS_ADMIN with 64 rows.
Inserting in Table DOC_TASAOCUOTA with 1 rows.
Inserting in Table PRY_PROYECTO_VERSIONES with 719 rows.
Inserting in Table PRY_REASIGNACION_APKAM with 412 rows.
Inserting in Table PRY_DOC_REPOSITORIO with 5 rows.
Inserting in Table INICIAL with 1439 rows.
Inserting in Table DOC_CONTABLE_DECIMALES with 3032 rows.
Inserting in Table PRY_PROYECTO_MONTOS_VERSIONES with 1831 rows.
Inserting in Table PRY_REL_KAM_PROYECTO with 310 rows.
Inserting in Table PRY_UR_PRESUPUESTO_VERSIONES with 8882 rows.
Inserting in Table PRY_UR_SERVICIO_VERSIONES with 2994 rows.
Inserting in Table PRY_RELACION_VERSIONES with 729 rows.
Inserting in Table COBROS_CONFIGURACION with 14 rows.
Inserting in Table DOC_REGISTRO_ANTERIORES with 4311 rows.
Inserting in Table FC_PAGOS_DETALLE_ANTERIORES with 32438 rows.
Inserting in Table FC_PAGOS_DOCUMENTO_ANTERIORES with 10541 rows.
Inserting in Table CG_PROCESOS_PERSONAS with 103 rows.
Inserting in Table CG_PERSONA_TIPO_PERSONA with 12196 rows.
Inserting in Table TMP_CORRECTOS with 1262 rows.
Inserting in Table CAMBIOS_PERIODO with 3 rows.
Inserting in Table AMPLIACIONES with 0 rows.
Inserting in Table CG_FUENTE_FINANCIAMIENTO with 0 rows.
Inserting in Table CG_ESTRUCTURA_PROGRAMATICA with 0 rows.
Inserting in Table FC_PAGOS_X_DOCUMENTO_DETALLE with 5050 rows.
Inserting in Table FC_PAGOS_X_DOCUMENTO with 1867 rows.
Inserting in Table FC_CONFIG_X_ENTIDAD_X_MOMENTO with 0 rows.
Inserting in Table FC_CONFIG_REGS_AUX with 0 rows.
Inserting in Table FC_CONFIG_CONST_X_ENTIDAD with 0 rows.
Inserting in Table DOC_CONFIG_REGS_AUX with 0 rows.
Inserting in Table IN_PRESUPUESTO with 7 rows.
Inserting in Table EXPEDIENTE with 598 rows.
Inserting in Table EG_TRANSACCION with 640 rows.
Inserting in Table EG_PRESUPUESTO_PLANILLA with 0 rows.
Inserting in Table EG_PRESUPUESTO with 1311 rows.
Inserting in Table EG_PLANILLA with 0 rows.
Inserting in Table EG_DET_PLANILLA with 0 rows.
Inserting in Table EG_DEDUCCION_PLANILLA with 0 rows.
Inserting in Table EG_ATRIBUTO_PRESUPUESTO with 23598 rows.
Inserting in Table DOCUMENTO_EXPEDIENTE with 589 rows.
Inserting in Table TE_CUENTAS_ESCRITURALES with 491 rows.
Inserting in Table TE_CAMPOS_CHEQUE with 9 rows.
Inserting in Table SG_REPORTES with 52979 rows.
Inserting in Table SG_OPERACIONES with 770747 rows
Ended process...
Thanks...
```
## Prerequisites

1. OpenJDK  8 installed in your system.
2. An oracle database running on a server.
3. A postgreSQL output running  on a server too. There the migration results will be placed.

## Built With
* [Maven](https://maven.apache.org/) - Dependency Management
* [JSQLParser](https://github.com/JSQLParser/JSqlParser) - Library

## Contributing

Please read [CONTRIBUTING.md](https://gitlab.com/dcchivela/idbtrans) for details on our code of conduct, and the process for submitting pull requests to us.

## License

This project is licensed under the LGLP License - see the [LICENSE.md](LICENSE.md) file for details


##Business Plan
Infotec provides iDBTrans support for every stage of the migration processes. Please feel free to ask about our service plans including:

1. Specialized advice on iDBTrans use.
2. Parallel data processing.
3. Specific questions about pitfalls in your migration.. 
4. Full support on the migration path.
5. Specialized development to meet specific needs.

For more information please contact to: vinculacion@infotec.mx.


## Authors
* **Daniel A. Cervantes Cabrera** - *Initial work* - [PurpleBooth](https://gitlab.com/dcchivela)
* **Marcos Romero Sandoval** - *Initial work* - [PurpleBooth](https://gitlab.com/marcos.romero)