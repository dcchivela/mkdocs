### Example migration


Source config file (config_source.json):

```
{
    "url":"jdbc:oracle:thin:@localhost:1521:ORCL3",
    "user":"SIO",
    "password":"SIO",		
 	"driver":"oracle.jdbc.driver.OracleDriver",
	"schemas":["SIO"] 
}
```


Tarjet config file (config_tarjet.json):
```
{
    "url":"jdbc:postgresql://localhost:5432/migrated",
	"user":"postgres",
	"password":"postgres",
	"driver":"org.postgresql.Driver",
	"schema":"SCHEMA"
}

```

Run:
```
java -jar idbtrans-0.0.jar -i ..\config\config_source.json -o ..\config\config_tarjet.json
```

```
--------------------------------------------------------------------------------------
--   ______   ____    ____    ______
--  /\__  _\ /\  _`\ /\  _`\ /\__  _\
--  \/_/\ \/ \ \ \/\ \ \ \L\ \/_/\ \/ _ __    __      ___     ____
--     \ \ \  \ \ \ \ \ \  _ <' \ \ \/\`'__\/'__`\  /' _ `\  /',__\
--      \_\ \__\ \ \_\ \ \ \L\ \ \ \ \ \ \//\ \L\.\_/\ \/\ \/\__, `\
--      /\_____\\ \____/\ \____/  \ \_\ \_\\ \__/.\_\ \_\ \_\/\____/
--      \/_____/ \/___/  \/___/    \/_/\/_/ \/__/\/_/\/_/\/_/\/___/
--
--                            Version  0.0.0
--                         DAIC -  INFOTEC CDMX

--------------------------------------------------------------------------------------

Loading dictionary...
Loading source data base file configuration...
Loading tarjet data base file configuration...
Connecting to source data base...
Connecting to tarjet data base...
Started at:Fri Mar 27 03:36:43 CST 2020 getting sequences...
Started at:Fri Mar 27 03:36:43 CST 2020 getting default values column...
Started at:Fri Mar 27 03:36:44 CST 2020 getting partitions...
Started at:Fri Mar 27 03:36:45 CST 2020 getting tables...
Started at:Fri Mar 27 03:36:45 CST 2020 Getting columns...
Started at:Fri Mar 27 03:37:55 CST 2020 getting Views...
Started at:Fri Mar 27 03:37:55 CST 2020 getting Materialized Views...
Started at:Fri Mar 27 03:37:55 CST 2020 getting Primary Keys...
Started at:Fri Mar 27 03:37:57 CST 2020 getting Foreign keys...
Started at:Fri Mar 27 03:39:32 CST 2020 getting constraints type check...
Started at:Fri Mar 27 03:39:33 CST 2020 getting indexes...
Started at:Fri Mar 27 03:39:33 CST 2020 getting objects code, procedures,packages,funtions...
Started at:Fri Mar 27 03:39:36 CST 2020 getting dbaObjects...
Started at:Fri Mar 27 03:39:36 CST 2020 getting pck procedures....
Started at:Fri Mar 27 03:39:36 CST 2020 getting code procedures package...
Started at:Fri Mar 27 03:39:38 CST 2020 Set code to procedures and functions ..
Started at:Fri Mar 27 03:39:52 CST 2020 Set code to triggers..
Started at:Fri Mar 27 03:39:53 CST 2020 Set objects type user defined for every schema..
Started at:Fri Mar 27 03:39:53 CST 2020 Setting status VALID or INVALID for objects of code
Start of migration...
Creating schemas and script for PostgreSQL..
Creating Sequences and script for PostgreSQL..
Creating tables and script for PostgreSQL..
Creating default values for tables and script for PostgreSQL..
Creating partitions and script for PostgreSQL..
Creating Indexes and script for PostgreSQL..
Creatting Functions and script for PostgreSQL..
Creatting Procedures and script for PostgreSQL..
Creatting packages and script for PostgreSQL..
Creatting triggers and script for PostgreSQL..
Creatting views and script for PostgreSQL..
Creatting materialized views and script for PostgreSQL..
Inserting data on tables for PostgreSQL..
Started at:Fri Mar 27 03:40:14 CST 2020
SCHEMA: SIO inserted rows:4
No. of Schemas: 1
-----------------------------------------
Infotec_Ora2Post: Migration Report
-----------------------------------------
        Input Data Base
-----------------------------------------
*********** BEGIN SCHEMA: SIO [1] de [1] ***********
No. of Tables to migrate : 807
No. of Indexes to migrate : 354
No. of Column Default: 603
No. of Views: 141
Status: VALIDS 122, INVALIDS 19
No. of Materilized Views: 39
Status: VALIDS 0, INVALIDS 39
No. of Functions: 996
Status: VALIDS 900, INVALIDS 96
No. of Procedures: 509
Status: VALIDS 374, INVALIDS 135
Packages :35 VALIDS : 24 INVALIDS:11 No. of Function and Procedures VALIDS: 366
No. of Triggers: 122 VALIDS: 80 INVALIDS:42
*********** END SCHEMA SIO ***********
-----------------------------------------
        Output Data Base
-----------------------------------------
No. of Schemas: 1
*********** BEGIN SCHEMA: SIO [1] de [1] ***********
No. of Tables: 824
        Status: Migrated 806 No migrated 18
No. of Index: 354
        Status: Migrated 311, No migrated 43
No. of Column Default: 603
        Status: Migrated 595, No migrated 8
No. of Views: 141
        Status: Migrated 89, No migrated 52 Converted:122 Not-converted:19
No. of Materialized Views: 39
        Status: Migrated 19, No migrated 20 Converted:30 Not-converted:9
No. of Functions: 996
        Status: Migrated 834, No migrated 162 Converted:0 Not-converted:996
No. of Procedures: 509
        Status: Migrated 298, No migrated 211 Converted:368 Not-converted:141
No. of Functions and Procedures Packages: 35
ALL PROCEDURES MIGRATED :0 NO MIGRATED:0
No. of Triggers: 122
        Status: Migrated 70, No migrated 52 Converted:80 Not-converted:42
*********** END SCHEMA SIO ***********
-----------------------------------------
Ended process...
Thanks...
```